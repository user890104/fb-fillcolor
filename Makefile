TARGET=main
CFLAGS=-Wall -Werror

all: $(TARGET)

clean:
	rm -rf $(TARGET)
