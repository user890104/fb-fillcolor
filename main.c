#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/fb.h>

int main(int argc, char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage: %s /dev/fbX <blue> <green> <red> <alpha>\n", argv[0]);
		return 1;
	}

	int fbfd = open(argv[1], O_RDWR);

	if (fbfd == -1) {
		perror("Error: cannot open framebuffer device");
		return 2;
	}

	puts("The framebuffer device was opened successfully");

	// Get fixed screen information
	struct fb_fix_screeninfo finfo;
	
	if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo) == -1) {
		perror("Error reading fixed information");
		return 3;
	}

	// Get variable screen information
	struct fb_var_screeninfo vinfo;
	
	if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo) == -1) {
		perror("Error reading variable information");
		return 4;
	}

	printf("%dx%d, %dbpp\n", vinfo.xres, vinfo.yres, vinfo.bits_per_pixel);
	
	if (vinfo.xres == 0 || vinfo.yres == 0 || vinfo.bits_per_pixel == 0) {
		fputs(stderr, "Invalid framebuffer info, exiting");
		return 5;
	}
	
	unsigned long int screensize = vinfo.xres * vinfo.yres * (vinfo.bits_per_pixel / 8);

	// Map the device to memory
	char *fbp = (char *)mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0);

	if ((int)fbp == -1) {
		perror("Error: failed to map framebuffer device to memory");
		return 6;
	}
	
	puts("The framebuffer device was mapped to memory successfully");
	
	size_t x, y;
	unsigned int value;
	unsigned char blue = atoi(argv[2]), green = atoi(argv[3]), red = atoi(argv[4]), alpha = atoi(argv[5]);
	
	if (vinfo.bits_per_pixel == 32) {
		value = alpha << 24 | red << 16 | green << 8 | blue;
	}
	
	if (vinfo.bits_per_pixel == 16) {
		value = ((red >> 3) << 11) | ((green >> 2) << 5) | (blue >> 3);
	}
	
	for (y = 0; y < vinfo.yres; ++y) {
		for (x = 0; x < vinfo.xres; ++x) {
			long int location = (x + vinfo.xoffset) * (vinfo.bits_per_pixel / 8) + (y + vinfo.yoffset) * finfo.line_length;
			
			if (vinfo.bits_per_pixel == 32) {
				*((unsigned int *)(fbp + location)) = value;
			}
			
			if (vinfo.bits_per_pixel == 16) {
				*((unsigned short *)(fbp + location)) = value;
			}
		}
	}
	
	munmap(fbp, screensize);
	close(fbfd);
	return 0;
}
